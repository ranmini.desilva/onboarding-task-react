import React, { useState } from "react";
import { List, ListItem, ListItemText, TextField, IconButton, ListItemSecondaryAction } from '@mui/material'
// import { Delete as DeleteIcon, Edit as EditIcon } from "@mui/material/icons";


const ItemList = () => { 

  const myItems = [
    {
      text: "Item 1",
      index: "1"
    },
    {
      text: "Item 2",
      index: "2"
    },
    {
      text: "Item 3",
      index: "3"
    },
    {
      text: "Item 4",
      index: "4"
    }
  ]

  const [items, setItems] = useState([]);
  const [editItemId, setEditItemId] = useState(null);
  const [editItemText, setEditItemText] = useState("");

  const handleAddItem = (event) => {
    event.preventDefault(); //prevents the default behavior of the form (refresh page)
    const newItem = { text: event.target.itemText.value };
    setItems([...items, newItem]);
    event.target.itemText.value = "";
  };

  const handleDeleteItem = (index) => {
    setItems([...items.slice(0, index), ...items.slice(index + 1)]);
  };

  const handleEditItem = (index, text) => {
    setEditItemId(index);
    setEditItemText(text);
  };


  return (
    <div>
       <h5>This is my item list</h5>

      {/* old list --------------------------------*/}
      <div>
              {/* <List container spacing={1}>
        <ListItem>
          <ListItemText primary="Item1"/>
        </ListItem>
        <ListItem>
          <ListItemText primary="Item2"/>
        </ListItem>
        <ListItem>
          <ListItemText primary="Item3"/>
        </ListItem>
        <ListItem>
          <ListItemText primary="Item4"/>
        </ListItem>
        <ListItem>
          <ListItemText primary="Item5"/>
        </ListItem>
      </List> */}
      </div> 

      {/* new list ----------------------------------*/}
      <List>
        {myItems.map((item, index) => (
          <ListItem className="" key={item.index} >
             <ListItemText primary={item.text}/>

             {editItemId === index ? (
                <>
                  <TextField
                    value={editItemText}
                    onChange={(event) => setEditItemText(event.target.value)}
                  />
                </>
              ) : ( 
              // dont know the above notation ? 

                <>
                {/* child elements on the right of the list item ---------------------------------- */}
                  <ListItemSecondaryAction> 
                    <IconButton
                      onClick={() => handleEditItem(index, item.text)}
                    >
                      {/* <EditIcon /> */}
                    </IconButton>

                    <IconButton
                      onClick={() => handleDeleteItem(index)}
                    >
                      {/* <DeleteIcon /> */}
                    </IconButton>

                  </ListItemSecondaryAction>
                </>
              )}

          </ListItem>
        ))}
      </List>

      <br></br>

          {/* use formika for this form --------------*/}
      <form onSubmit={handleAddItem}> 
        <TextField name="itemText" label="Add Item" style={{padding: "15px"}}/>
      </form>

    </div>
  )
}
export default ItemList;