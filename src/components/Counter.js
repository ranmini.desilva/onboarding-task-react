import { Button, Grid, Typography } from '@mui/material';
import React, { useState, useEffect } from 'react'
//import { FormControl } from '@mui/material';
import './Counter.css';



export default function Counter() {

    const [count, setCount]=useState(0);
    useEffect(()=> {}, [count]); // added the count variable

  return (
    <div>
      This is my counter
      <div className="container">
      <Grid container spacing={4}>
        <Grid item>
            <Button className="button" variant="contained" color="success" onClick={()=> setCount((c) => c-1)}>
                -
            </Button>

        </Grid>
        
        {/* <FormControl id="my-count" aria-describedby="counter-text" value={count}>
        </FormControl> */}

        <Grid item>
            <Typography variant="h6" value={count}>{count}</Typography>
        </Grid>
        
        <Grid item>
            <Button className="button" variant="contained" color="success" onClick={()=> setCount((c) => c+1)}>
                 +
             </Button>
        </Grid>
      
      </Grid>
      </div>
    </div>
  )
}
