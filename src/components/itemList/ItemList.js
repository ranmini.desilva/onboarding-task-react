import React, { useState } from "react";
import { List, ListItem, ListItemText, TextField, IconButton, ListItemSecondaryAction } from '@mui/material'
import { Delete as DeleteIcon, Edit as EditIcon } from "@mui/icons-material";
import { Formik, Form, Field } from "formik";
import { Button } from "@mui/material";


const ItemList = () => { 

  // const myItems = [
  //   {
  //     text: "Item 1"
  //   },
  //   {
  //     text: "Item 2"
  //   },
  //   {
  //     text: "Item 3"
  //   },
  //   {
  //     text: "Item 4"
  //   }
  // ]

  // const [items, setItems] = useState(myItems);//ok

  const [items, setItems] = useState([]); //Initially state for items should be an empty array([]) since we want to add items dynamically.
  const [editItemId, setEditItemId] = useState(null);
  const [editItemText, setEditItemText] = useState("");


  //create a new item object with the entered text and add it to the existing items state.-->
  // const handleAddItem = (event) => {
  //   event.preventDefault(); //prevents the default behavior of the form (refresh page)
  //   const newItem = {
  //     text: event.target.itemText.value,
  //     index: items.length + 1 // Generate a unique index
  //   };
  //   setItems([...items, newItem]);
  //   event.target.itemText.value = ""; // Clear the input field.
  // };


  //to accept the item text as a parameter
  const handleAddItem = (itemText) => {
    const newItem = {
      text: itemText,
      index: items.length + 1,
    };
    setItems([...items, newItem]);
  };
  
  // const handleDeleteItem = (index) => {
  //   setItems([...items.slice(0, index), ...items.slice(index + 1)]); //remove an item from the items array based on its index.
  // };

  const handleDeleteItem = (index) => {
    setItems(items.filter((_, i) => i !== index)); //use the filter method to create a new array excluding the item to be deleted.
  };
  
  const handleEditItem = (index, text) => {
    setEditItemId(index);
    setEditItemText(text);
  };

  const handleSaveEditItem = (index) => {
    const updatedItems = [...items];
    updatedItems[index].text = editItemText;
    setItems(updatedItems);
    setEditItemId(null);
    setEditItemText("");
  };

  return (
    <div>
    <h5>This is my item list</h5>

    <List>
      {items.map((item, index) => (
        <ListItem className="" key={item.index}>
          {editItemId === index ? (
            <>
              <TextField
                value={editItemText}
                onChange={(event) => setEditItemText(event.target.value)}
              />
              <IconButton onClick={() => handleSaveEditItem(index)}>
                Save
              </IconButton>
            </>
          ) : (
            <>
              <ListItemText primary={item.text} />
              <ListItemSecondaryAction>

                <IconButton onClick={() => handleEditItem(index, item.text)}>
                  <EditIcon />
                </IconButton>

                <IconButton onClick={() => handleDeleteItem(index)}>
                  <DeleteIcon />
                </IconButton>

              </ListItemSecondaryAction>
            </>
          )}
        </ListItem>
      ))}
    </List>

    <br />
    <h5>Add New Items below</h5>
      <div>
        <Formik
          initialValues={{ itemText: "" }}
          onSubmit={(values, { resetForm }) => {
            handleAddItem(values.itemText);
            resetForm();
          }}
        >
          <Form>
            <Field
              as={TextField}
              name="itemText"
              label="Enter item to add"
              style={{ padding: "15px" }}
            />
            <Button type="submit">Add to List</Button>
          </Form>
        </Formik>
      </div>

  </div>
  )
}
export default ItemList;