import './App.css';
import Counter from './components/counter/Counter';
import ItemList from './components/itemList/ItemList';


function App() {
  return (
    <div className="App">
      <h2>My React JS Task 1</h2>
      <Counter/>
      <hr></hr>
      <br></br>

      <h2>My React JS Task 2</h2>
      <ItemList/>
    

    </div>
  );
}

export default App;
